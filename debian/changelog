obs-studio (22.0.3+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches: Refresh patches.
  * debian/control: Update Standards-Version.
  * debian/rules: Disable updater with the correct flag.
  * debian/: Enable scripting support. (Closes: #904480)
  * debian/libobs0.symbols: Update symbols.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 16 Dec 2018 18:14:30 +0100

obs-studio (21.1.2+dfsg1-1) unstable; urgency=medium

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Sebastian Ramacher ]
  * New upstream release.
  * debian/patches: Refresh patches.
  * debian/control: Bump Standards-Version.
  * debian/libobs0.symbols: Add new symbols.

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 29 May 2018 21:19:37 +0200

obs-studio (21.0.2+dfsg1-1) unstable; urgency=medium

  [ Sebastian Ramacher ]
  * New upstream release.
    - Fix building with ffmpeg 3.5. (Closes: #888361)
  * debian/patches:
    - Refresh patches.
    - Do not display license on first startup. (Closes: #867756)
  * debian/control: Bump Standards-Version.
  * debian/: Bump debhelper compat to 11.
  * debian/libobs0.symbols: Update symbols.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org.
  * d/copyright: Use https protocol in Format field.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 19 Feb 2018 22:35:33 +0100

obs-studio (19.0.3+dfsg1-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release.
  * debian/control:
    - Remove Build-Conflicts. No longer needed.
    - Bump Standards-Version.
  * debian/libobs0.symbols: Add new symbols.
  * debian/copyright: Update using decopy.

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 29 Jun 2017 21:18:29 +0200

obs-studio (18.0.1+dfsg1-2) experimental; urgency=medium

  * debian/rules: Add B-C for Qt >= 5.8 to work around #858762.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 23 Apr 2017 15:51:32 +0200

obs-studio (18.0.1+dfsg1-1) experimental; urgency=medium

  * New upstream release. (Closes: #854055)
  * debian/copyright:
    - Update using decopy.
    - Exclude unused embedded code copies.
  * debian/{control,compat,rules}: Bump debhelper compat to 10.
  * debian/libobs0.symbol: Add new symbols.
  * debian/patches: Refresh.
  * debian/rules: Disable update and libcaption.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 19 Apr 2017 23:59:37 +0200

obs-studio (0.15.4+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Add libspeexdsp-dev and libvlc-dev to B-D.
    - Add vlc to obs-plugin's Recommends.
  * debian/libobs0.symbols: Update symbols.
  * debian/copyright: Update copyright information
  * debian/{rules,obs.rst}: Add manpage.
  * debian/{rules,control}: Tighten inter-package dependencies.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 28 Aug 2016 16:07:28 +0200

obs-studio (0.14.2+dfsg1-3) unstable; urgency=medium

  * debian/control. Also restrict obs-plugins to any-amd64.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 02 Jul 2016 08:39:10 +0200

obs-studio (0.14.2+dfsg1-2) unstable; urgency=medium

  * debian/control: Restrict Architecture to any-amd64.
  * debian/copyright: Document Blackmagic Design copyright.

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 01 Jul 2016 19:23:10 +0200

obs-studio (0.14.2+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * debian/libobs0.symbols: Add new symbols.
  * debian/control:
    - Bump Standards-Version.
    - Add libasound2-dev to Build-Depends.
    - Mention in Description minimal OpenGL requirement.
  * debian/copyright:
    - Update Files-Excluded for new upstream version.
    - Add license information for new files.
    - Fix license information for libff and w32-pthreads.

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 24 May 2016 22:56:30 +0200

obs-studio (0.13.2+dfsg1-1) unstable; urgency=medium

  * Initial release (Closes: #774744)

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 25 Feb 2016 20:42:33 +0100
